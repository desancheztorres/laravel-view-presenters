<?php

namespace App\Presenters\User;

use App\Presenters\Presenter;
use Illuminate\Support\Str;

class UserSubscriptionPresenter extends Presenter {

    public function currentPlan() {
        // logic
        return Str::title('monthly');
    }
}
